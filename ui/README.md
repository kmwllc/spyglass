# SpyGlass

## Features

.

* View your Solr data quickly in a simple UI with minimal configuration

## Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Docker](https://www.docker.com/products/docker-desktop)

```bash
# Clone this repository
$ git clone git@bitbucket.org:kwatters/brigade-search.git

# Go into the repository
$ cd brigade-search

# Build and run the application
$ docker/scripts/run_dev.sh
```

## Configure

TBD

## License
Apache v2.0
