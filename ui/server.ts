/***************************************************************************************************
 * Load `$localize` onto the global scope - used if i18n tags appear in Angular templates.
 */
import '@angular/localize/init';
import 'zone.js/dist/zone-node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import { join } from 'path';

import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';

import { createProxyMiddleware } from 'http-proxy-middleware';

// The Express app is exported so that it can be used by serverless Functions.
export function app(): express.Express {
  const server = express();
  const distFolder = join(process.cwd(), 'dist/spyglass/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine('html', ngExpressEngine({
    bootstrap: AppServerModule
  }));

  server.set('view engine', 'html');
  server.set('views', distFolder);

  server.get('*.*', (req, res, next) => {  
    req.url = req.url.replace(/\/spyglass/, '');    
    next();
  }, express.static(distFolder, {
    maxAge: '1y',
    fallthrough: false
  }));

  // // Example Express Rest API endpoints
  // // server.get('/api/**', (req, res) => { });
  // // Serve static files from /browser
  // server.get('*.*', express.static(distFolder, {
  //   maxAge: '1y',
  //   fallthrough: false
  // }));
    
  const configProxy = createProxyMiddleware('/api/config', { 
    //target: 'http://localhost:8000',
    target: 'http://localhost:3000',
    pathRewrite: {"^/api" : ""},
    changeOrigin: true
  });

  server.use('/api/config', configProxy);

  const solrProxy = createProxyMiddleware('/api/solr', { 
    //target: 'http://localhost:8000',
    target: 'http://awsu1-65lu1a01d.amer.thermo.com:8983',
    pathRewrite: {"^/api" : ""},
    changeOrigin: true,    
  });

  server.use('/api/solr', solrProxy);


  // All regular routes use the Universal engine
  server.get('*', (req, res) => {    
    res.render(indexHtml, { req, providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }] });
  });

  return server;
}

function run(): void {
  const port = process.env.PORT || 4000;

  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
