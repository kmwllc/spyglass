#!/bin/bash

docker build . -f docker/DockerFile -t spyglass

docker-compose -p spyglass-demo -f docker/docker-compose.yml down
docker-compose -p spyglass-demo -f docker/docker-compose.yml up -d

echo 'waiting for solr to be ready'
docker cp docker/scripts/wait-for-it.sh spyglass-solr1:tmp/.
docker exec spyglass-solr1 /tmp/wait-for-it.sh -h solr1 -p 8983 -t 60
echo 'solr is ready'

#copy the configset to solr filesystem
docker cp docker/configsets spyglass-solr1:tmp/.
#upload the configset to zookeeper
docker exec spyglass-solr1 solr zk upconfig -z zoo1:2181,zoo2:2181,zoo3:2181 -n _dev -d /tmp/configsets/dev_config
#create the collection
docker exec spyglass-solr1 curl 'http://spyglass-solr1:8983/solr/admin/collections?action=CREATE&name=_dev&numShards=1&replicationFactor=1&wt=json&collection.configName=_dev'
#copy the data to the solr filesystem
docker cp docker/dev-data/imdb-movies.csv spyglass-solr1:tmp/.
#index the data into the collection with the post tool
docker exec spyglass-solr1 post -c _dev /tmp/imdb-movies.csv -type text/csv -params 'fieldnames=id,title_txt,original_title_txt,year_keyword,date_published_keyword,genre_txt,duration_i,country_keyword,language_keyword,director_txt,writer_txt,production_company_txt,actors_txt,description_txt,avg_vote_d,votes_i,budget_d,usa_gross_income_d,worldwide_gross_income_d,metascore_d,reviews_from_users_d,reviews_from_critics_d&header=true&f.writer_txt.split=true&f.director_txt.split=true&f.genre_txt.split=true&trim=true&literal._type=movie&skip=budget_d,worldwide_gross_income_d,usa_gross_income_d'
