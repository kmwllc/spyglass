import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {ResultsComponent} from './spyglass-search/results/results.component';
import {BoxComponent} from './spyglass-search/box/box.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule, NgbModule],
      declarations: [
        AppComponent, ResultsComponent, BoxComponent
      ],
    }).compileComponents();
  }));
});
