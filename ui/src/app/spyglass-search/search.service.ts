import {Injectable} from '@angular/core';
import {Refinement, SearchContext} from './shared/search-context';
import {BehaviorSubject} from 'rxjs';
import {SolrService} from './solr.service';
import {ErrorWrapper, SolrError, SolrResponse} from './shared/solr-response';
import {Result} from './shared/result';
import {SolrHeader} from './shared/solr-header';
import {Facets} from "./shared/facets";
import {ConfigService} from "./config.service";

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  //initial search context
  private defaultContext = new SearchContext();
  private searchContextSource = new BehaviorSubject<SearchContext>(this.defaultContext);
  currentSearchContext = this.searchContextSource.asObservable();

  private defaultResult = {'docs': [], maxScore: 0, numFound: 0, start: 0};
  private resultSource = new BehaviorSubject<Result>(this.defaultResult);
  currentResults = this.resultSource.asObservable();

  private defaultHeader = {QTime: 0, status: 0}
  private resultHeaderSource = new BehaviorSubject<SolrHeader>(this.defaultHeader);
  currentResultHeader = this.resultHeaderSource.asObservable();

  private defaultHighlights = {};
  private highlightsSource = new BehaviorSubject<Object>(this.defaultHighlights);
  currentHighlights = this.highlightsSource.asObservable();

  private defaultFacets = {count: 0};
  private facetsSource = new BehaviorSubject<Facets>(this.defaultFacets);
  currentFacets = this.facetsSource.asObservable();

  private defaultError = undefined;
  private errorSource = new BehaviorSubject<SolrError | undefined>(this.defaultError);
  currentError = this.errorSource.asObservable();

  private loading = new BehaviorSubject<boolean>(false);
  isLoading = this.loading.asObservable();

  constructor(private solrService: SolrService, private configService: ConfigService) {
    configService.getConfig().subscribe(configResponse => {
      if (configResponse) {
        this.defaultContext = SearchContext.newSearchContext(configResponse);
        this.searchContextSource.next(this.defaultContext);
      }
    });
  }

  changeSearch(searchContext: SearchContext) {
    this.loading.next(true);
    this.solrService.select(searchContext).subscribe((solrResponse: SolrResponse) => {
      this.searchContextSource.next(searchContext);
      this.resultSource.next(solrResponse.response);
      this.resultHeaderSource.next(solrResponse.responseHeader);
      this.highlightsSource.next(solrResponse.highlighting);
      this.facetsSource.next(solrResponse.facets);
      this.errorSource.next(solrResponse.error);
      this.loading.next(false);
    }, (wrapper: ErrorWrapper) => {
      this.searchContextSource.next(searchContext);
      this.resultSource.next(this.defaultResult);
      this.resultHeaderSource.next(this.defaultHeader);
      this.highlightsSource.next(this.defaultHighlights);
      this.facetsSource.next(this.defaultFacets);
      this.errorSource.next(wrapper.error.error);
      this.loading.next(false);
    });
  }

  changeQuery(query: string) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.query = query;
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

  changeRows(rows: number) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.rows = rows;
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

  changeStart(start: number) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.start = start;
    this.changeSearch(newSearch);
  }
  
  changeSort(sort: string) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.sort = sort;
    this.changeSearch(newSearch);
  }

  changeDefType(defType: string) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.defType = defType;
    this.searchContextSource.next(newSearch);
  }

  refine(refinement: Refinement) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.refine.push(refinement);
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

  refineMultiple(field: string, refinements: Array<Refinement>) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.refine = newSearch.refine.filter((refinement) => {
      return refinement.field != field;
    });
    refinements.forEach(refinement => {
      newSearch.refine.push(refinement);
    });
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

  removeRefinement(index: number) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.refine.splice(index, 1);
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

  resetAll() {
    const newSearch = this.searchContextSource.getValue();
    newSearch.query = "*";
    newSearch.refine = [];
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

  resetFacets() {
    const newSearch = this.searchContextSource.getValue();
    newSearch.refine = [];
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

  putFilter(key: string, filter: string) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.filters.set(key, filter);
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

  removeFilter(key: string) {
    const newSearch = this.searchContextSource.getValue();
    newSearch.filters.delete(key);
    newSearch.start = 0;
    this.changeSearch(newSearch);
  }

}
