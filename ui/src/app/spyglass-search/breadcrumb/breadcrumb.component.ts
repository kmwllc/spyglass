import { Component, OnInit } from '@angular/core';
import {SearchService} from "../search.service";
import {Refinement} from "../shared/search-context";

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  refine: Array<Refinement>;
  query: string;
  isLoading: boolean;

  constructor(private searchService: SearchService) {
    this.refine = [];
    this.isLoading = false;
    this.query = '';
  }

  ngOnInit(): void {
    this.searchService.currentSearchContext.subscribe(searchContext => {
      this.refine = searchContext.refine;
      this.query = searchContext.query;
    });
    this.searchService.isLoading.subscribe(isLoading => {
      this.isLoading = isLoading;
    });
  }

  removeRefinement(index: number) {
    this.searchService.removeRefinement(index);
  }
}
