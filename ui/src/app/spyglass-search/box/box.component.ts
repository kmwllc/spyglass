import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SearchService} from '../search.service';
import {SearchContext} from '../shared/search-context';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

  searchForm: FormGroup;
  searchContext: SearchContext = new SearchContext();

  constructor(private fb: FormBuilder, private searchService: SearchService) {
    this.searchForm = this.fb.group({
      query: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.searchService.currentSearchContext.subscribe(searchContext => {
      this.searchContext = searchContext;
      this.searchForm.setValue({'query': this.searchContext.query});
    });
  }

  onSubmit() {
    const formModel = this.searchForm.value;
    this.searchService.changeQuery(formModel.query as string);    
  }

}
