import {Component, OnInit} from '@angular/core';
import {Result} from '../shared/result';
import {SearchService} from '../search.service';
import {SearchContext} from '../shared/search-context';
import {ConfigService} from '../config.service';
import {ConfigResponse, ResultCard} from '../shared/config-response';
import {ResultMapping} from '../shared/result-mapping';
import { ColumnMapping } from '../shared/column-mapping';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  result: Result;
  searchContext: SearchContext;
  resultMapping: ResultMapping;
  columnMapping: Array<ColumnMapping>;
  resultLayout: ResultCard;
  highlights: any;
  page: number;
  isLoading: boolean;
  isShowing: boolean;
  config: ConfigResponse;

  constructor(private searchService: SearchService, private configService: ConfigService) {
    this.result = new Result();
    this.searchContext = new SearchContext();
    this.resultMapping = new ResultMapping();
    this.resultLayout = new ResultCard();
    this.columnMapping = [];
    this.highlights = {};
    this.page = 0;
    this.isLoading = false;
    this.isShowing = false;
    this.config = new ConfigResponse();
  }

  ngOnInit() {
    this.configService.getConfig().subscribe(configResponse => {
      if(configResponse) {
        this.config = configResponse;
        if(configResponse.resultLayout) {
          this.resultLayout = configResponse.resultLayout;
        }
        else if(configResponse.resultColumns) {
          this.columnMapping = configResponse.resultColumns;
        } else if (configResponse.resultFields) {
          this.resultMapping = configResponse.resultFields;
        }
      }
    });
    this.searchService.currentResults.subscribe(result => {
      this.result = result;
      this.isShowing = false;
    });
    this.searchService.currentSearchContext.subscribe(searchContext => {
      this.searchContext = searchContext;
      this.page = (searchContext.start / searchContext.rows) + 1;
    });
    this.searchService.currentHighlights.subscribe(highlights => {
      this.highlights = highlights;
    });
    this.searchService.isLoading.subscribe(isLoading => this.isLoading = isLoading);
  }

}
