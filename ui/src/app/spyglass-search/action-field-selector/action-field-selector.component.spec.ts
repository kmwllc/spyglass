import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionFieldSelectorComponent } from './action-field-selector.component';

describe('ActionFieldSelectorComponent', () => {
  let component: ActionFieldSelectorComponent;
  let fixture: ComponentFixture<ActionFieldSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionFieldSelectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionFieldSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
