import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleFieldComponent } from './google-field/google-field.component';
import { ResultsComponent } from './results/results.component';
import { BoxComponent } from './box/box.component';
import { PaginationComponent } from './pagination/pagination.component';
import { FacetComponent } from './facet/facet.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { InfoComponent } from './info/info.component';
import { FacetNavComponent } from './facet-nav/facet-nav.component';
import { ResultCardComponent } from './result-card/result-card.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ActionsComponent } from './actions/actions.component';
import { ActionFieldSelectorComponent } from './action-field-selector/action-field-selector.component';
import { ResetComponent } from './reset/reset.component';
import { NumberFieldComponent } from './number-field/number-field.component';
import { CurrencyFieldComponent } from './currency-field/currency-field.component';
import { SpyglassComponent } from './spyglass/spyglass.component';
import { DateFieldComponent } from './date-field/date-field.component';
import { LinkFieldComponent } from './link-field/link-field.component';
import { CustomFieldComponent } from './custom-field/custom-field.component';
import { CustomFieldDirective } from './custom-field.directive';



@NgModule({
  declarations: [
    GoogleFieldComponent,
    ResultsComponent,
    BoxComponent,
    PaginationComponent,
    FacetComponent,
    BreadcrumbComponent,
    InfoComponent,
    FacetNavComponent,
    ResultCardComponent,
    ActionsComponent,
    ActionFieldSelectorComponent,
    ResetComponent,
    NumberFieldComponent,
    CurrencyFieldComponent,
    SpyglassComponent,
    DateFieldComponent,
    LinkFieldComponent,
    CustomFieldComponent,
    CustomFieldDirective
  ],
  imports: [
    CommonModule,
    BrowserModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    GoogleFieldComponent,
    ResultsComponent,
    BoxComponent,
    PaginationComponent,
    FacetComponent,
    BreadcrumbComponent,
    InfoComponent,
    FacetNavComponent,
    ResultCardComponent,
    ActionsComponent,
    ActionFieldSelectorComponent,
    ResetComponent,
    NumberFieldComponent,
    CurrencyFieldComponent,
    SpyglassComponent,
    CommonModule,
    BrowserModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule
  ]
})
export class SpyglassSearchModule { }
