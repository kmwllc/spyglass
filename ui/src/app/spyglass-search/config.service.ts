import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConfigResponse} from './shared/config-response';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private scopeSource = new BehaviorSubject<string | null>(null);
  currentScope = this.scopeSource.asObservable();

  private configSource = new BehaviorSubject<ConfigResponse | null>(null);
  currentConfig = this.configSource.asObservable();

  constructor(private http: HttpClient) {}



  getConfig(scope?: string | null) {
    if(scope && scope !== this.scopeSource.value) {
      this.scopeSource.next(scope);
      this.configSource.next(null);
    }
    if(!this.configSource.value && this.scopeSource.value) {
      this.http.get<ConfigResponse>('/api/config/' + this.scopeSource.value).subscribe(config => {
        this.configSource.next(config);
      });
    }
    return this.currentConfig;
  }

}
