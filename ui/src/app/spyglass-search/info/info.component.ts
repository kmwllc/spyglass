import { Component, OnInit } from '@angular/core';
import {SearchService} from "../search.service";
import {SolrError} from "../shared/solr-response";

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {
  start: number;
  rows: number;
  numFound: number;
  qTime: number;
  isLoading: boolean;
  error?: SolrError;

  constructor(private searchService: SearchService) {
    this.start = 0;
    this.rows = 0;
    this.numFound = 0;
    this.qTime = 0;
    this.isLoading = false;
  }

  ngOnInit(): void {
    this.searchService.currentSearchContext.subscribe(searchContext => {
      this.start = searchContext.start;
      this.rows = searchContext.rows;
    });
    this.searchService.currentResults.subscribe(results => {
      this.numFound = results.numFound;
    });
    this.searchService.currentResultHeader.subscribe(header => {
      if(header) {
        this.qTime = header.QTime;
      }
    });
    this.searchService.currentError.subscribe(error => {
      this.error = error;
    })
    this.searchService.isLoading.subscribe(isLoading => {
      this.isLoading = isLoading;
    });
  }

}
