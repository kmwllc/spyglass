import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';
import { ShoppingCart } from '../shared/shopping-cart';
import { ShoppingCartService } from '../shopping-cart.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  constructor(private searchService: SearchService, private cartService: ShoppingCartService) { }

  ngOnInit(): void {
  }

  resetAll() {
    this.searchService.resetAll();
    this.cartService.changeCart(new ShoppingCart());
  }

  resetFacets() {
    this.searchService.resetFacets();
  }
}
