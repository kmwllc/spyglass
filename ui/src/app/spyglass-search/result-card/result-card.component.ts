import {Component, Input, OnInit} from '@angular/core';
import {ConfigResponse, FieldView, ResultCard} from "../shared/config-response";
import { CartItem, ShoppingCart } from '../shared/shopping-cart';
import { ShoppingCartService } from '../shopping-cart.service';

@Component({
  selector: 'app-result-card',
  templateUrl: './result-card.component.html',
  styleUrls: ['./result-card.component.css']
})
export class ResultCardComponent implements OnInit {

  @Input() doc: any;
  @Input() highlight: any;
  @Input() layout: ResultCard = new ResultCard();
  @Input() config: ConfigResponse = new ConfigResponse();

  toggleText: string;
  showDetails: boolean;
  cartText: string;
  cartImage?: string;
  inCart: boolean;


  constructor(public shoppingCartService: ShoppingCartService) {
    this.toggleText = 'Show details...'
    this.showDetails = false;
    this.cartText = '';
    this.inCart = false;
  }

  ngOnInit(): void {
    this.inCart = false;
    this.cartText = this.config.cart.addText;
    this.cartImage = this.config.cart.addImage;
    this.shoppingCartService.currentShoppingCart.subscribe(cart => {
      if(this.inCart && !cart.items.has(this.doc['id'])) {
        this.inCart = false;
        this.cartText = this.config.cart.addText;
        this.cartImage = this.config.cart.addImage;
      } else if(!this.inCart && cart.items.has(this.doc['id'])) {
        this.inCart = true;
        this.cartText = this.config.cart.removeText;
        this.cartImage = this.config.cart.removeImage;
      }
    });
  }

  toggleDetails(): void {
    if(this.showDetails) {
      this.showDetails = false;
      this.toggleText = 'Show details...';
    } else {
      this.showDetails = true;
      this.toggleText = 'Hide details...';
    }
  }

  toggleCart(): void {
    if(this.inCart) {
      this.shoppingCartService.remove(this.doc['id']);
    } else {
      const cartItem = new CartItem();
      cartItem.id = this.doc['id'];
      this.shoppingCartService.add(cartItem);
    }
  }
}
