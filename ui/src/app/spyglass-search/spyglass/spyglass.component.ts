import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigService } from '../config.service';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-spyglass',
  templateUrl: './spyglass.component.html',
  styleUrls: ['./spyglass.component.css']
})
export class SpyglassComponent implements OnInit {

  constructor(private route: ActivatedRoute, private configService: ConfigService, private searchService: SearchService) { }

  ngOnInit(): void {
    const params = this.route.snapshot.queryParamMap;
    const q = params.get('q');
    const scope = params.get('scope') || 'default';
    this.configService.getConfig(scope).subscribe(config => {
      if(config) {
        if(q) {
          this.searchService.changeQuery(q);
        }
      }
    });
  }
}
