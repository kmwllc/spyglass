import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpyglassComponent } from './spyglass.component';

describe('SpyglassComponent', () => {
  let component: SpyglassComponent;
  let fixture: ComponentFixture<SpyglassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpyglassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpyglassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
