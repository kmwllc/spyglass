import {Result} from './result';
import {Facets} from './facets';
import {SolrHeader} from './solr-header';

export class SolrResponse {
  [index: string]: any;
  response: Result;
  facets: Facets;
  responseHeader: SolrHeader;
  highlighting: Object;
  error?: SolrError;

  constructor() {
    this.response = new Result();
    this.facets = new Facets();
    this.responseHeader = new SolrHeader();
    this.highlighting = {};
  }
}

export class SolrError {
  code?: number;
  metadata?: Array<string>;
  msg?: string;
}

export class ErrorWrapper {
  error: SolrResponse

  constructor() {
    this.error = new SolrResponse();
  }
}
