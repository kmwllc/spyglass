export class ColumnMapping {
    label: string;
    field: string;
    text_align: string;
    width: number;

    constructor() {
        this.label = '';
        this.field = '';
        this.text_align = '';
        this.width = 0;
    }
}
