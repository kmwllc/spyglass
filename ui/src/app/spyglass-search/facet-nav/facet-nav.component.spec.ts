import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FacetNavComponent } from './facet-nav.component';

describe('FacetNavComponent', () => {
  let component: FacetNavComponent;
  let fixture: ComponentFixture<FacetNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FacetNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FacetNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
