import {NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { SpyglassSearchModule } from './spyglass-search/spyglass-search.module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import {APP_BASE_HREF} from '@angular/common';

@NgModule({
  declarations: [    
    AppComponent
  ],
  imports: [
    SpyglassSearchModule,
    AppRoutingModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' })
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: '/spyglass'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
