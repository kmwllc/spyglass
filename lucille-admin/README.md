# LucilleAdmin

How to start the LucilleAdmin application
---

Build and run the program with the docker scripts.

```shell
./docker/scripts/build_dev.sh && ./docker/scripts/run_dev.sh
```

Curator Discovery Server
---

The demo exposes a Curator Discovery Server endpoint that manages the services that are monitored by the UI.
See: `https://curator.apache.org/curator-x-discovery-server/index.html`

The local endpoints are documented in a swagger UI which will be running at `http://localhost:8080`

User Interface
---

The demo is available at `http://localhost:8181/lucille-admin/index.html`. This page will connect to a websocket at
`http://localhost:8000` which is broadcasting out whenever their are changes to the services registered in zookeeper.

