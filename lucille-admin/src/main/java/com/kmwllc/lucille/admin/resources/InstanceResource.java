package com.kmwllc.lucille.admin.resources;

import com.kmwllc.lucille.admin.discovery.InstanceDetails;
import org.apache.curator.x.discovery.server.rest.DiscoveryContext;
import org.apache.curator.x.discovery.server.rest.DiscoveryResource;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.ContextResolver;

@Path("/discovery")
public class InstanceResource extends DiscoveryResource<InstanceDetails> {
  public InstanceResource(@Context ContextResolver<DiscoveryContext<InstanceDetails>> resolver) {
    super(resolver.getContext(DiscoveryContext.class));
  }
}
