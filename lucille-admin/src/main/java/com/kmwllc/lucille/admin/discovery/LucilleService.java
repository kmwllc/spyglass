package com.kmwllc.lucille.admin.discovery;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.x.discovery.*;
import org.apache.curator.x.discovery.details.JsonInstanceSerializer;

import java.io.Closeable;

public class LucilleService implements Closeable {
  private final ServiceDiscovery<InstanceDetails> serviceDiscovery;
  private final ServiceInstance<InstanceDetails> thisInstance;

  public LucilleService(CuratorFramework client, String path, String serviceName, String description) throws Exception {
    UriSpec uriSpec = new UriSpec("{scheme}://{address}:{port}");

    thisInstance = ServiceInstance.<InstanceDetails>builder()
      .name(serviceName)
      .id("1")
      .payload(new InstanceDetails(description))
      .port(8181)
      .address("localhost")
      .uriSpec(uriSpec)
      .serviceType(ServiceType.PERMANENT)
      .build();

    JsonInstanceSerializer<InstanceDetails> serializer = new JsonInstanceSerializer<>(InstanceDetails.class);

    serviceDiscovery = ServiceDiscoveryBuilder.builder(InstanceDetails.class)
      .client(client)
      .basePath(path)
      .serializer(serializer)
      .thisInstance(thisInstance)
      .build();
  }

  public ServiceInstance<InstanceDetails> getThisInstance() {
    return thisInstance;
  }

  public void start() throws Exception {
    serviceDiscovery.start();
  }

  @Override
  public void close() {
    CloseableUtils.closeQuietly(serviceDiscovery);
  }

}
