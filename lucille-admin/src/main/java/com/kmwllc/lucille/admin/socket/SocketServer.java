package com.kmwllc.lucille.admin.socket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kmwllc.lucille.admin.discovery.InstanceDetails;
import com.kmwllc.lucille.admin.discovery.LucilleDiscoveryService;
import io.dropwizard.lifecycle.Managed;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.x.discovery.ServiceCache;
import org.apache.curator.x.discovery.details.ServiceCacheListener;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public class SocketServer extends WebSocketServer implements Managed {
  private static final Logger log = LoggerFactory.getLogger(SocketServer.class);
  private final LucilleDiscoveryService discovery;
  private ServiceCache<InstanceDetails> cache;
  private final ObjectMapper mapper = new ObjectMapper();

  public SocketServer(InetSocketAddress address, LucilleDiscoveryService discovery) {
    super(address);
    this.discovery = discovery;
  }

  @Override
  public void onOpen(WebSocket conn, ClientHandshake handshake) {
    conn.send("Welcome to the server!");
    log.info("new connection to " + conn.getRemoteSocketAddress());
  }

  @Override
  public void onClose(WebSocket conn, int code, String reason, boolean remote) {
    log.info("closed {} with exit code {} reason: reason", conn.getRemoteSocketAddress(), code, reason);
  }

  @Override
  public void onMessage(WebSocket conn, String message) {
    log.info("received message from {}: {}", conn.getRemoteSocketAddress(), message);
  }

  @Override
  public void onMessage( WebSocket conn, ByteBuffer message ) {
    System.out.println("received ByteBuffer from "	+ conn.getRemoteSocketAddress());
  }

  @Override
  public void onError(WebSocket conn, Exception e) {
    log.error("an error occured on connection {}", conn.getRemoteSocketAddress(), e);
  }

  @Override
  public void onStart() {
    log.info("socket server started successfully");
    this.cache = discovery.getCache();
    this.cache.addListener(new ServiceCacheListener() {
      @Override
      public void cacheChanged() {
        log.info("socket cache changed");
        try {
          broadcast(mapper.writeValueAsString(cache.getInstances()));
        } catch (JsonProcessingException e) {
          log.error("failed to write service cache update to socket", e);
        }
      }

      @Override
      public void stateChanged(CuratorFramework curatorFramework, ConnectionState connectionState) {
        log.info("cache state changed: {}", connectionState);
      }
    });
  }

}
