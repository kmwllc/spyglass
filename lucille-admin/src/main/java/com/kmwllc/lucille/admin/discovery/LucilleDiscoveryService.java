package com.kmwllc.lucille.admin.discovery;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import io.dropwizard.lifecycle.Managed;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.x.discovery.*;
import org.apache.curator.x.discovery.details.JsonInstanceSerializer;
import org.apache.curator.x.discovery.details.ServiceCacheListener;
import org.apache.curator.x.discovery.server.rest.DiscoveryContext;
import org.apache.curator.x.discovery.strategies.RandomStrategy;
import org.apache.curator.x.discovery.strategies.RoundRobinStrategy;
import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ext.ContextResolver;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LucilleDiscoveryService implements Managed, ContextResolver<DiscoveryContext<InstanceDetails>> {
  private final static Logger log = LoggerFactory.getLogger(LucilleDiscoveryService.class);
  private CuratorFramework client;
  private ServiceDiscovery<InstanceDetails> serviceDiscovery;
  private ServiceCache<InstanceDetails> cache;
  private final ObjectMapper mapper = new ObjectMapper();
  Map<String, ServiceProvider<InstanceDetails>> providers = new ConcurrentHashMap<>();

  public LucilleDiscoveryService() {
    log.info("creating!!!");
  }

  @Override
  public void start() throws Exception {
    log.info("starting!!!");
    client = CuratorFrameworkFactory.newClient("zoo1:2181,zoo2:2181,zoo3:2181", new ExponentialBackoffRetry(1000, 3));
    client.start();

    JsonInstanceSerializer<InstanceDetails> serializer = new JsonInstanceSerializer<>(InstanceDetails.class);
    serviceDiscovery =
      ServiceDiscoveryBuilder.builder(InstanceDetails.class).client(client).basePath("/discovery").serializer(serializer).build();
    serviceDiscovery.start();

    cache = serviceDiscovery.serviceCacheBuilder().name("simple").build();
    cache.start();
    cache.addListener(new ServiceCacheListener() {
      @Override
      public void cacheChanged() {
        log.info("The cache has changed.");
        try {
          log.info(mapper.writeValueAsString(cache.getInstances()));
        } catch (JsonProcessingException e) {
          log.error("Error writing out cached instances", e);
        }
      }

      @Override
      public void stateChanged(CuratorFramework curatorFramework, ConnectionState connectionState) {
        log.info("State Changed: {}", connectionState);
      }
    });

    List<LucilleService> services = Lists.newArrayList();
    addInstance(new String[]{"foo", "a foo instance"}, client, "add foo a foo instance", services);
    listInstances(serviceDiscovery);

    log.info("" + serviceDiscovery.queryForInstances("foo").size());
  }

  @Override
  public void stop() {
    for (ServiceProvider<InstanceDetails> cache : providers.values()) {
      CloseableUtils.closeQuietly(cache);
    }
    CloseableUtils.closeQuietly(cache);
    CloseableUtils.closeQuietly(serviceDiscovery);
    CloseableUtils.closeQuietly(client);
  }

  public ServiceCache<InstanceDetails> getCache() {
    return cache;
  }

  private static void processCommands(ServiceDiscovery<InstanceDetails> serviceDiscovery, Map<String,
    ServiceProvider<InstanceDetails>> providers, CuratorFramework client) throws Exception {
    // More scaffolding that does a simple command line processor

    printHelp();

    List<LucilleService> servers = Lists.newArrayList();
    try {
      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
      boolean done = false;
      while (!done) {
        System.out.print("> ");

        String line = in.readLine();
        if (line == null) {
          break;
        }

        String command = line.trim();
        String[] parts = command.split("\\s");
        if (parts.length == 0) {
          continue;
        }
        String operation = parts[0];
        String args[] = Arrays.copyOfRange(parts, 1, parts.length);

        if (operation.equalsIgnoreCase("help") || operation.equalsIgnoreCase("?")) {
          printHelp();
        } else if (operation.equalsIgnoreCase("q") || operation.equalsIgnoreCase("quit")) {
          done = true;
        } else if (operation.equals("add")) {
          addInstance(args, client, command, servers);
        } else if (operation.equals("delete")) {
          deleteInstance(args, command, servers);
        } else if (operation.equals("random")) {
          listRandomInstance(args, serviceDiscovery, providers, command);
        } else if (operation.equals("list")) {
          listInstances(serviceDiscovery);
        }
      }
    } finally {
      for (LucilleService server : servers) {
        CloseableUtils.closeQuietly(server);
      }
    }
  }

  private static void listRandomInstance(String[] args, ServiceDiscovery<InstanceDetails> serviceDiscovery,
                                         Map<String, ServiceProvider<InstanceDetails>> providers, String command) throws Exception {
    // this shows how to use a ServiceProvider
    // in a real application you'd create the ServiceProvider early for the service(s) you're interested in

    if (args.length != 1) {
      System.err.println("syntax error (expected random <name>): " + command);
      return;
    }

    String serviceName = args[0];
    ServiceProvider<InstanceDetails> provider = providers.get(serviceName);
    if (provider == null) {
      provider =
        serviceDiscovery.serviceProviderBuilder().serviceName(serviceName).providerStrategy(new RandomStrategy<InstanceDetails>()).build();
      providers.put(serviceName, provider);
      provider.start();

      Thread.sleep(2500); // give the provider time to warm up - in a real application you wouldn't need to do this
    }

    ServiceInstance<InstanceDetails> instance = provider.getInstance();
    if (instance == null) {
      System.err.println("No instances named: " + serviceName);
    } else {
      outputInstance(instance);
    }
  }

  private static void listInstances(ServiceDiscovery<InstanceDetails> serviceDiscovery) throws Exception {
    // This shows how to query all the instances in service discovery

    try {
      Collection<String> serviceNames = serviceDiscovery.queryForNames();
      log.info(serviceNames.size() + " type(s)");
      for (String serviceName : serviceNames) {
        Collection<ServiceInstance<InstanceDetails>> instances = serviceDiscovery.queryForInstances(serviceName);
        log.info(serviceName);
        for (ServiceInstance<InstanceDetails> instance : instances) {
          outputInstance(instance);
        }
      }

    } catch (KeeperException.NoNodeException e) {
      log.error("There are no registered instances.");
    } finally {
      CloseableUtils.closeQuietly(serviceDiscovery);
    }
  }

  private static void outputInstance(ServiceInstance<InstanceDetails> instance) {
    log.info("\t" + instance.getPayload().getDescription() + ": " + instance.buildUriSpec() + ", status: " + instance.getPayload().getStatus());
  }

  private static void deleteInstance(String[] args, String command, List<LucilleService> servers) {
    // simulate a random instance going down
    // in a real application, this would occur due to normal operation, a crash, maintenance, etc.

    if (args.length != 1) {
      System.err.println("syntax error (expected delete <name>): " + command);
      return;
    }

    final String serviceName = args[0];
    LucilleService server = Iterables.find
      (
        servers,
        new Predicate<LucilleService>() {
          @Override
          public boolean test(LucilleService lucilleService) {
            return false;
          }

          @Override
          public boolean apply(LucilleService server) {
            return server.getThisInstance().getName().endsWith(serviceName);
          }
        },
        null
      );
    if (server == null) {
      System.err.println("No servers found named: " + serviceName);
      return;
    }

    servers.remove(server);
    CloseableUtils.closeQuietly(server);
    System.out.println("Removed a random instance of: " + serviceName);
  }

  private static void addInstance(String[] args, CuratorFramework client, String command,
                                  List<LucilleService> servers) throws Exception {
    // simulate a new instance coming up
    // in a real application, this would be a separate process

    if (args.length < 2) {
      log.error("syntax error (expected add <name> <description>): " + command);
      return;
    }

    StringBuilder description = new StringBuilder();
    for (int i = 1; i < args.length; ++i) {
      if (i > 1) {
        description.append(' ');
      }
      description.append(args[i]);
    }

    String serviceName = args[0];
    LucilleService server = new LucilleService(client, "/discovery", serviceName, description.toString());
    servers.add(server);
    server.start();

    log.info(serviceName + " added");
  }

  private static void printHelp() {
    System.out.println("An example of using the ServiceDiscovery APIs. This example is driven by entering commands at" +
      " the prompt:\n");
    System.out.println("add <name> <description>: Adds a mock service with the given name and description");
    System.out.println("delete <name>: Deletes one of the mock services with the given name");
    System.out.println("list: Lists all the currently registered services");
    System.out.println("random <name>: Lists a random instance of the service with the given name");
    System.out.println("quit: Quit the example");
    System.out.println();
  }

  @Override
  public DiscoveryContext<InstanceDetails> getContext(Class<?> aClass) {
    return new DiscoveryContext<InstanceDetails>() {

      @Override
      public int getInstanceRefreshMs() {
        return 30000;
      }

      @Override
      public ServiceDiscovery<InstanceDetails> getServiceDiscovery() {
        return serviceDiscovery;
      }

      @Override
      public void marshallJson(ObjectNode objectNode, String field, InstanceDetails instanceDetails) {
        if (instanceDetails == null) {
          instanceDetails = new InstanceDetails();
        }
        objectNode.putPOJO(field, instanceDetails);
        log.info(objectNode.toString());
      }

      @Override
      public InstanceDetails unMarshallJson(JsonNode jsonNode) throws Exception {
        InstanceDetails payload;
        payload = mapper.readValue(jsonNode.toString(), InstanceDetails.class);
        return payload;
      }

      @Override
      public ProviderStrategy<InstanceDetails> getProviderStrategy() {
        return new RoundRobinStrategy<>();
      }
    };
  }
}
