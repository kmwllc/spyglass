package com.kmwllc.lucille.admin.socket;

import io.dropwizard.lifecycle.Managed;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

public class SocketTestClient extends WebSocketClient implements Managed {
  private static final Logger log = LoggerFactory.getLogger(SocketTestClient.class);

  public SocketTestClient(URI serverUri, Draft draft) {
    super(serverUri, draft);
  }

  public SocketTestClient(URI serverUri) {
    super(serverUri);
  }

  @Override
  public void onOpen(ServerHandshake serverHandshake) {
    log.info("new client connection opened");
  }

  @Override
  public void onMessage(String s) {
    log.info("received: {}", s);
  }

  @Override
  public void onClose(int i, String s, boolean b) {
    log.info("client closed iwth exit code {} reason: {}", i, s);
  }

  @Override
  public void onError(Exception e) {
    log.error("socket client error", e);
  }

  @Override
  public void start() throws Exception {
    this.connect();
  }

  @Override
  public void stop() throws Exception {
    this.close();
  }
}
