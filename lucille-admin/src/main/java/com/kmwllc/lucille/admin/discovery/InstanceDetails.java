package com.kmwllc.lucille.admin.discovery;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("details")
public class InstanceDetails {
  private String description;
  private InstanceStatus status;

  public InstanceDetails() {
    this("");
  }

  public InstanceDetails(String description) {
    this.description = description;
    this.status = InstanceStatus.stopped;
  }

  public String getDescription() {
    return description;
  }

  public InstanceStatus getStatus() {
    return this.status;
  }

  public void setStatus(InstanceStatus status) {
    this.status = status;
  }

  public static enum InstanceStatus {
    stopped,starting,running
  }
}
