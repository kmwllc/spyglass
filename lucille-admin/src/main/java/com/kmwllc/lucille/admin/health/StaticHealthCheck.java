package com.kmwllc.lucille.admin.health;

import com.codahale.metrics.health.HealthCheck;

public class StaticHealthCheck extends HealthCheck {
  @Override
  protected Result check() {
    return HealthCheck.Result.healthy();
  }
}
