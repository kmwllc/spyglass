package com.kmwllc.lucille.admin;

import com.kmwllc.lucille.admin.discovery.LucilleDiscoveryService;
import com.kmwllc.lucille.admin.health.StaticHealthCheck;
import com.kmwllc.lucille.admin.resources.InstanceResource;
import com.kmwllc.lucille.admin.socket.SocketServer;
import com.kmwllc.lucille.admin.socket.SocketTestClient;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.EnumSet;

public class LucilleAdminApplication extends Application<LucilleAdminConfiguration> {


  public static void main(final String[] args) throws Exception {
    new LucilleAdminApplication().run(args);
  }

  @Override
  public String getName() {
    return "LucilleAdmin";
  }

  @Override
  public void initialize(final Bootstrap<LucilleAdminConfiguration> bootstrap) {
    bootstrap.addBundle(new AssetsBundle("/assets", "/"));
  }

  @Override
  public void run(final LucilleAdminConfiguration configuration,
                  final Environment environment) throws Exception {
    final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

    cors.setInitParameter("allowedOrigins", "*");
    cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
    cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");
    cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");


    LucilleDiscoveryService lucilleDiscoveryService = new LucilleDiscoveryService();
    environment.lifecycle().manage(lucilleDiscoveryService);
    environment.healthChecks().register("static", new StaticHealthCheck());
    InstanceResource resource = new InstanceResource(lucilleDiscoveryService);
    environment.jersey().register(resource);

    SocketServer socket = new SocketServer(new InetSocketAddress(8000), lucilleDiscoveryService);
    environment.lifecycle().manage(socket);

    SocketTestClient client = new SocketTestClient(new URI("ws://localhost:8000"));
    environment.lifecycle().manage(client);
  }


}
